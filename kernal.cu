#include <math.h>

	struct inum {
			float r;
			float i;
	};

	__device__ float inum_abs (inum a) {
		return sqrt(a.r*a.r + a.i*a.i);
	}


	__device__ inum inum_sqr(inum a) {
		inum retval;
		retval.r = a.r*a.r - a.i*a.i;
		retval.i = 2 * a.r * a.i;
		return retval;
	}


	__device__ inum inum_add(inum a, inum b) {
		inum retval;
		retval.r = a.r + b.r;
		retval.i = a.i + b.i;
		return retval;
	}


	__global__ void make_image(float* img, float* rows, float* cols, int width, int hight, int maxIt)
	{
		int col = blockIdx.x * blockDim.x + threadIdx.x;
		int row = blockIdx.y * blockDim.y + threadIdx.y;
        int threadId = row * width + col;
		if (!(row < hight && col < width)) {
			img[threadId] = -1;
			return;
		} 
		
		inum c;
		img[threadId] = threadId;
		/*
		c.r = rows[col];
		c.i = cols[width - row];
		inum z;
		z.r = c.r;
		z.i = c.i;
		int iterations = maxIt;
		for(int n = 0; n < maxIt; n++) {
			if(inum_abs(z) > 2) {
				iterations = n;
				break;
			}
			z = inum_add(inum_sqr(z), c);
		}
		img[threadId] = iterations;*/
	}

int main() {
    return 0;
}